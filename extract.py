#!/usr/bin/python

# Import frameworks
from CoreData import *
from Foundation import *

from geojson import MultiPoint, LineString, Feature, Point, FeatureCollection


import sys,os

# MOLoader is a python classed derived from NSManagedObject. It takes an
# entity name, managed object context and dictionary of key/value pairs
# and instantiates an instance of the entity within the context and
# sets the key/value pairs.
class MOLoader(NSManagedObject):

    def initWithEntityNamed_inManagedObjectContext_withDictionary_(self, entityName, moc, data):
        entity = NSEntityDescription.entityForName_inManagedObjectContext_(entityName, moc)
        self = super(MOLoader, self).initWithEntity_insertIntoManagedObjectContext_(entity, moc)
        if self is None:
            return None

        for key in data.keys():
            self.setValue_forKey_(data[key], key)

        return self

def main():

    #print 'Using Managed Object Model', sys.argv[1]
    #print 'Creating CoreData file', sys.argv[2]
    #print 'Input data', sys.argv[3]

    # Load model
    bundle = NSBundle.bundleWithPath_(sys.argv[1])
    bundleArray = NSArray.arrayWithObject_(bundle)

    mom = NSManagedObjectModel.alloc().initWithContentsOfURL_(NSURL.fileURLWithPath_(sys.argv[1]))
    if mom is None:
        print "Unable to load model"
        sys.exit(1)

    # Setup persistent store
    urlForDb = NSURL.fileURLWithPath_(sys.argv[2])
    psc = NSPersistentStoreCoordinator.alloc().initWithManagedObjectModel_(mom)
    ps = psc.addPersistentStoreWithType_configuration_URL_options_error_(NSSQLiteStoreType, None, urlForDb, None, None)

    if ps[0] is None:
        print ps[1].description()
        sys.exit(1)

    moc = NSManagedObjectContext.alloc().init()
    moc.setPersistentStoreCoordinator_(psc)

    entityDescription = NSEntityDescription.entityForName_inManagedObjectContext_(u'CDLocationEvent', moc)
    #print entityDescription

    #CDLocationEvent.bestDisplayTitleUnformatted
    for name in mom.entitiesByName():
        if name == u'CDLocationEvent':
            entityDescription = NSEntityDescription.entityForName_inManagedObjectContext_(name, moc)
            #print entityDescription

    #NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:@"Entity"];

    fetchRequest = NSFetchRequest.alloc().init()
    fetchRequest.setEntity_(entityDescription)

    results, errors_i_think = moc.executeFetchRequest_error_(fetchRequest, None)
    map_it(results)


def map_it(events):
    features = []
    for event in events:
        #print "%s (%s)" % (event.bestTitle(), event.timezone())
        #print "\t%s [%s, %s] %s" % (event.startTime(), event.startLatitude(), event.startLongitude(), event.endTime())
        lat = event.startLatitude()
        lon = event.startLongitude()
        if lat and lon:
            newFeature = Feature(geometry=Point((lon,lat)), id=event.remoteID(), properties={"time": str(event.startTime()), "title": event.bestDisplayTitle()})
            features.append(newFeature)
    print FeatureCollection(features)


if __name__ == '__main__':
  main()
