// Playground - noun: a place where people can play

import Cocoa
import Foundation
import CoreData
import MapKit


var helloWorld = "Hello, World"

let locationPath = NSBundle.mainBundle().pathForResource("LocationTrackerModel", ofType:"momd")
let storePath = NSBundle.mainBundle().pathForResource("persistentStore", ofType:"")
var errorMsg = ""


//let fileManager = NSFileManager.defaultManager()
//let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
//var locationURL = documentsPath[0].URLByAppendingPathComponent("LocationTrackerModel.momd")


@objc(CDLocationEvent) // make compatible with objective-c
class CDLocationEvent : NSManagedObject
{
    @NSManaged var bestDisplayTitle: String!
    @NSManaged var startTime: NSDate
    @NSManaged var startLatitude: NSNumber
    @NSManaged var startLongitude: NSNumber
}

class Pin: NSObject, MKAnnotation {
    var coordinate:CLLocationCoordinate2D
    var title:String = ""
    var subTitle:String = ""
    
    init(coordinates:CLLocationCoordinate2D, placeName:String, description:String){
        self.title = placeName
        self.subTitle = description
        self.coordinate = coordinates
    }
    
}

func map(events:AnyObject[]) {
    var pins:Pin[] = []
    for event:AnyObject in events {
        var title = event.bestDisplayTitle
        var ts = event.startTime
        var lat = event.startLatitude.doubleValue
        var lon = event.startLongitude.doubleValue
        if Int(lat) != 0 && Int(lon) != 0 {
            let pin = Pin(coordinates: CLLocationCoordinate2D(latitude: lat, longitude:lon), placeName: title, description: "")
            pins += pin
        } else {
            println("lat: \(lat) / lon: \(lon)")
        }
    }
    var c = pins.count
    var p = pins
    MKMapItem.openMapsWithItems(pins, launchOptions:nil)

}


func parse(momd:String, storeFile:String) {
    var error:NSError?
    
    // Load model
    let bundle = NSBundle(path:momd)
    let bundleArray = NSArray(object:bundle)
    //let mom = NSManagedObjectModel.mergedModelFromBundles(nil)
    let mom = NSManagedObjectModel(contentsOfURL:NSURL(fileURLWithPath:momd))
    
    // Setup persistent store
    let psc = NSPersistentStoreCoordinator(managedObjectModel:mom)
    let ps = psc.addPersistentStoreWithType(NSSQLiteStoreType, configuration:nil, URL:NSURL(fileURLWithPath:storeFile), options:nil, error:&error)
    
    if let e = error {
        println("write failure: \(e.localizedDescription)")
    }
    
    var moc: NSManagedObjectContext = NSManagedObjectContext()
    moc.persistentStoreCoordinator = psc
    
    var entityDescription = NSEntityDescription.entityForName("CDLocationEvent", inManagedObjectContext: moc)
    
    //CDLocationEvent.bestDisplayTitleUnformatted
    var entities:NSEntityDescription[] = mom.entities as NSEntityDescription[]
    
    for entity:NSEntityDescription in entities {
        var name = entity.name
        if name == "CDLocationEvent" {
            entityDescription = NSEntityDescription.entityForName(name, inManagedObjectContext: moc)
            entityDescription = entity
        }
    }
    
    var fetchLocation = NSFetchRequest(entityName: "CDLocationEvent")
    var fetchRequest = NSFetchRequest()
    fetchRequest.entity = entityDescription
    fetchRequest.fetchLimit = 10

    var results = moc.executeFetchRequest(fetchRequest, error:&error)
    if let e = error {
        println("write failure: \(e.localizedDescription)")
    }
    
    map(results)
}


if let locationPath = NSBundle.mainBundle().pathForResource("LocationTrackerModel", ofType:"momd") {
    if let storePath = NSBundle.mainBundle().pathForResource("persistentStore", ofType:"") {
        parse(locationPath, storePath)
    } else {
        errorMsg = "storePath could not be found"
    }
} else {
    errorMsg = "locationPath could not be found"
}

println(errorMsg)
