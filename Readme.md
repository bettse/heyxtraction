# Geo Extraction

## Goal
To have some fun playing with the sqlite db I extracted from the a passive location tracking app

## Requirements

afcclient

## Ideas
 * Heat map of places I've visited and length of time
 * Animation/scrubbing/static visual of my travels
 * Map with images at locations
 * Image slideshow by time
 * Something that looks like a calendar of the week (day columns), but the columns are really a map with the days movements.  The map would be warped to make the path a stright line down the center.  Might not be able to handle loops.  Might looks terrible

